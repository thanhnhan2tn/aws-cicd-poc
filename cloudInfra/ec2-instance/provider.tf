provider "aws" {
  region     = "us-east-2"
}

provider "github" {
  token    = var._tf_git_oauth_token_
  base_url = "https://api.github.com/"
  organization = "edjortega"
}
