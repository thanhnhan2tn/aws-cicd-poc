//EC2 Container Registry (ecr) Variables

variable "shared_iam_ecr" {
  type = string
  description = "Path of the shared ecr json file"
  default = "../files/iam/ecr"
}

variable "ecr_repo_name" {
  type = list
  description = "Name of ECR repo name"
}

variable "ecrfullservice_ecr_service_name" {
  type = string
  description = "Name of the ECR services that will be allow for"
}

variable "_ec2role_name_"{
  type = string
  description = "Name of the ecr role name"
}
