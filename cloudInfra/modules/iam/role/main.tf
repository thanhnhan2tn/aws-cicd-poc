
resource "aws_iam_role" "ec2role" {
  name = var._ec2role_name_
  assume_role_policy = file("../files/iam/ec2-access.json")
}

resource "aws_iam_instance_profile" "ec2Profile" {
  name  = var._ec2role_profile_name_
  role  = aws_iam_role.ec2role.name
}
