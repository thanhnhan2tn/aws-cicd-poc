
variable "_ec2role_name_" {
   type =  string
   description = "EC2 Role Name from which will attach those policies"
}

variable "_ec2role_profile_name_" {
   type =  string
   description = "EC2 Profile Name from which the role will attach"
}

