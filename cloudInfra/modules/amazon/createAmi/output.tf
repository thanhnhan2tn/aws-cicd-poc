
output "my-public-ipaddr" {
 value = aws_instance.ami-instance.*.public_ip
}

output "ec2id" {
  value = aws_instance.ami-instance.id
}
