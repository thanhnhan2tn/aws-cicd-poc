#!/usr/bin/python3
# mysql-connector-python 

import mysql.connector
import redis

from redis import Redis
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='ip-172-31-16-44.us-east-2.compute.internal',
                                         database='mydb',
                                         user='root',
                                         password='23cedd390480ed6d9fa73e948840421d')

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

    redis_host = "ip-172-31-28-129.us-east-2.compute.internal"
    r = Redis(redis_host, socket_connect_timeout=1)
    r.ping()

except Error as e:
    print("Error while connecting ", e)



finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
    print('connected to redis "{}"'.format(redis_host)) 
